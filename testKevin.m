%% 

fac=1;

delta=1/fac;
N=1000*fac;


e=ones(N-1,1);
ee=ones(N,1);
% D=spdiags([-e e],0:1,N-1,N);
D=spdiags([-e e],0:1,N-1,N);


c1=1;
c2=0.5;

c=c1*e;
% cdd=c1*ee;


startSL=400*fac;
endSL=600*fac;


c(startSL:endSL)=c2;
% c([startSL-1 endSL+1])=(c2+c1)/2;

% cdd((startSL:endSL)+1)=c2;
% cdd=cdd(:);
% c([800 1200])=(1+2)/2;

C=spdiags([c],0,N-1,N-1);
Cinv=spdiags([c.^(-1)],0,N-1,N-1);
e1=eye(N,1);
b=e1+flipud(e1);
B=-spdiags([b],0,N,N);

e2=eye(N-1,1);
b2=c(1)*e2+flipud(e2)*c(end);
B2=-spdiags(b2,0,N-1,N-1);


Wmh=1/sqrt(delta)*spdiags([ee],0,N,N);
Wmh(1,1)=sqrt(2)/sqrt(delta);
Wmh(N,N)=sqrt(2)/sqrt(delta);
Wmh=Wmh;

Wh_mh=spdiags([e],0,N-1,N-1)/sqrt(delta);

A=[ (0*B2+zeros(N-1,N-1)) C*Wh_mh*D*Wmh; -Wmh*D'*C*Wh_mh B*Wmh*Wmh];

% A2D_3=[ (zeros(N-1,N-1)) Wh_mh*C*D*Wmh; -Wmh*D'*C*Wh_mh Wmh*Wmh*B];

tic;
[V,E]=eig(full(A));
toc;
% [V2,E2]=eig(full(A2D_3));


M=[-ones(N-1,1);ones(N,1)];
M=spdiags(M,0,2*N-1,2*N-1);

ll=(M*A)'-M*A;
norm(ll(:))

%%

delT=(endSL-startSL+1)/c2*delta;
np=-50:50;

analPoles=- 1/(delT)*log((c1+c2)/abs(c1-c2))+np*pi/delT*1i;

realPartPol=- 1/(delT)*log((c1+c2)/abs(c1-c2));

figure(1)
clf
% subplot(211)
plot(diag(E),'.')
hold on
plot(analPoles,'ro')
grid on
xlabel('real part eigenvalue')
ylabel('imaginary part eigenvalue')
set(gca,'fontsize',16)
legend('Numerical eigenvalues','Analytical Eigenvalues')

xwindow=realPartPol*[1.01 0.95];
pos=[ 0.17 0.59 0.35 0.35];
z = axes('position',pos);
box on 
plot(diag(E),'.')
axis([xwindow(1) xwindow(2) 0 0.25]);
hold on
plot(analPoles,'ro')
grid on



%%
P1=eye(2*N-1,2*N-1);
P2=eye(2*N-1,2*N-1);
P1(N:end,:)=[];
P2(1:(N-1),:)=[];

[~,ind]=(sort(abs(diag(E))));

funcNR=2;

figure(909)
subplot(121)
plot(c(:).*real(P1*V(:,ind(funcNR))))
title('u')
title('real part $u$')
grid on

subplot(122)
plot(real(P2*V(:,ind(funcNR))))
title('real part $\hat u$')
grid on


%%

funcNR=10;

figure(909)
clf

subplot(2,2,1)
plot(c(:).*real(P1*V(:,ind(funcNR))))
title('u')
title('real part $u$')
grid on

subplot(2,2,3)
plot(real(P2*V(:,ind(funcNR))))
title('real part $\hat u$')
grid on

subplot(2,2,[2 4])
plot(diag(E),'.')
hold on
plot(analPoles,'ro')
plot(diag(E(funcNR,funcNR)),'kx')
grid on
xlabel('real part eigenvalue')
ylabel('imaginary part eigenvalue')
set(gca,'fontsize',16)
legend('Numerical eigenvalues','Analytical Eigenvalues','Shown pole')
title(['Pole current ' num2str(((E(funcNR,funcNR))))])


xwindow=realPartPol*[1.01 0.95];
pos=[ 0.61 0.15 0.25 0.25];
z = axes('position',pos);
box on 
plot(diag(E),'.')


axis([xwindow(1) xwindow(2) 0 0.25]);
hold on
plot(analPoles,'ro')
plot(diag(E(ind(funcNR),ind(funcNR))),'kx','MarkerSize',10)
plot(conj(diag(E(ind(funcNR),ind(funcNR)))),'kx','MarkerSize',10)
grid on

%%
Ed=diag(E);

indexTr=find(imag(Ed)>-3e-3);

[~,ind2]=sort(abs(imag(Ed(indexTr))),'ascend');
ind=indexTr(ind2);


%%
funcNR=15;

uplot=c(:).*P1*V(:,ind(funcNR));
uhplot=P2*V(:,ind(funcNR));
lambda_pl=diag(E(ind(funcNR),ind(funcNR)));

disp(lambda_pl)


figure(909)
    subplot(2,2,[2 4])
    hold off
    plot(diag(E),'.')
    hold on
    plot(analPoles,'ro')
    plot(lambda_pl,'kx')
    grid on
    xlabel('real part eigenvalue')
    ylabel('imaginary part eigenvalue')
    set(gca,'fontsize',16)
    legend('Numerical eigenvalues','Analytical Eigenvalues','Shown pole')
    title(['Pole current ' num2str(((E(funcNR,funcNR))))])


    xwindow=realPartPol*[1.01 0.95];
    pos=[ 0.61 0.15 0.25 0.25];
    z = axes('position',pos);
    box on 
    plot(diag(E),'.')


    axis([xwindow(1) xwindow(2) 0 0.25]);
    hold on
    plot(analPoles,'ro')
    plot(lambda_pl,'kx','MarkerSize',10)
    plot(conj(lambda_pl),'kx','MarkerSize',10)
    grid on

%%
for t=0:300

    figure(909)
    

    subplot(2,2,1)
    hold off
    plot(real(uplot*exp(2*pi*1i*t/(2*36))))
    title('u')
    title('real part $u$')
    grid on
    hold on
	plot(imag(uplot*exp(2*pi*1i*t/(2*36))))
     
    ymax=max(abs(uplot));
    axis([0 N -ymax ymax])

    line([startSL startSL],[-ymax ymax],'Color','r')
    line([endSL endSL],[-ymax ymax],'Color','r')
    
    
    subplot(2,2,3)
    hold off
    plot(real(uhplot*exp(2*pi*1i*t/(2*36))))
    hold on
    plot(imag(uhplot*exp(2*pi*1i*t/(2*36))))
    title('real part $\hat u$')
    grid on
    ymax=max(abs(uhplot));

    axis([0 N -ymax ymax])
    line([startSL startSL],[-ymax ymax],'Color','r')
    line([endSL endSL],[-ymax ymax],'Color','r')

    
    

    
    pause(0.01)
end
    
%% redooing single absorbing boundary

N=500;
e=ones(N,1);
D_p2d=spdiags([-e e],0:1,N,N);
D_d2p=-D_p2d';

en=flipud(eye(N,1));
c=e;
c(1:200)=0.5;
C=diag(c);

W=diag(e);
W(N,N)=0.5;
What=diag(e);
What(1,1)=0.5;

A=[zeros(N,N) What\(C\D_d2p); W\D_p2d en*en'*c(end)];

[V,LamE]=eig(full(A));

%%

Lam=diag(LamE);
iteri=12

figure(1)
subplot(121)
plot(Lam,'x')
subplot(122)
lam_d=Lam;
[indx]=find(imag(lam_d)<0.003);
[~,ind]=sort(abs(imag(lam_d(indx))),'ascend');
plot(real(V(1:N,indx(ind(iteri)))))
title(num2str(lam_d(indx(ind(iteri)))))

%% old code

N=500;
e=ones(N,1);
c=e;
c(1:200)=0.5;
C=diag(c);

eex=[ones(N,1) -ones(N,1)]';
eex=eex(:);

A2D=spdiags([eex -eex],[-1 1],2*N,2*N);
A2D(2*N,2*N)=-c(end);

dex=[deltaX./(c(:).^2) -deltaX*ones(N,1)]';
dex(1,1)=dex(1,1)/2;
dex(end,end)=dex(end,end)/2;disp('HALF???')
dex=dex(:);
D2D=spdiags(dex(:),0,2*N,2*N);

[V,Lam]=eig(full(D2D\A2D));

%%
iteri=12

P1=eye(2*N,2*N);
P2=eye(2*N,2*N);
P2(2:2:end,:)=[];
P1(1:2:end,:)=[];

figure(1)
subplot(121)
plot(diag(Lam),'x')
subplot(122)
lam_d=(diag(Lam));
[indx]=find(imag(lam_d)<0.003);
[~,ind]=sort(abs(imag(lam_d(indx))),'ascend');
plot(P1*real(V(:,indx(ind(iteri)))))
title(num2str(lam_d(indx(ind(iteri)))))

%% old code adapt soubble

N=1000;
e=ones(N,1);
c=e;
c(401:601)=0.5;
C=diag(c);

eex=[ones(N,1) -ones(N,1)]';
eex=eex(:);
eex=eex(2:end);

A2D=spdiags([eex -eex],[-1 1],2*N-1,2*N-1);
A2D(1,1)=-c(1);
A2D(2*N-1,2*N-1)=-c(end);

dex=[deltaX./(c(:).^2) -deltaX*ones(N,1)]';
dex=dex(:);
dex=dex(2:end);

dex(1)=dex(1)/2;
dex(end)=dex(end)/2;disp('HALF???')
D2D=spdiags(dex(:),0,2*N-1,2*N-1);

[V,Lam]=eig(full(D2D\A2D));

%%
iteri=12

P1=eye(2*N-1,2*N-1);
P2=eye(2*N-1,2*N-1);
P2(2:2:end,:)=[];
P1(1:2:end,:)=[];

figure(1)
subplot(121)
plot(diag(Lam),'x')
subplot(122)
lam_d=(diag(Lam));
[indx]=find(imag(lam_d)<0.003);
[~,ind]=sort(abs(imag(lam_d(indx))),'ascend');
plot(P1*real(V(:,indx(ind(iteri)))))
title(num2str(lam_d(indx(ind(iteri)))))
%%




%% transform both variants into eachother
A2D_2=[ (zeros(N-1,N-1)) -Wh_mh*Wh_mh*C*C*D; Wmh*Wmh*D' Wmh*Wmh*B];

pert=D2D\A2D;
PP=[P1;P2];
pert=PP*pert*PP';

%%
figure(2)
subplot(221),spy(pert)
subplot(223),spy(pert-A2D_2)
subplot(222),plot(eig(pert),'x')
subplot(224),plot(eig(full(A2D_2)),'x')

%%
A2D_2=[ (zeros(N-1,N-1)) Wh_mh*Wh_mh*C*C*D; -Wmh*Wmh*D' -Wmh*Wmh*B];
A2D_3=[ (zeros(N-1,N-1)) Wh_mh*C*D*Wmh; -Wmh*D'*C*Wh_mh -Wmh*Wmh*B];


figure(34)
plot(eig(full(A2D_3)),'x')